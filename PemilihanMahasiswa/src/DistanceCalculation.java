public class DistanceCalculation {

    private static final float[] divider = {10f, 10f, 10f, 5f, 3f, 4f, 1f, 3f, 1f};

    public static float calculateDistance(float[][] reference, float[][] result)
    {
        return matricesDistance(reference, result);
    }

    private static float vectorSquareDistance(float[] vector1, float[] vector2)
    {
        float result = 0f;
        for(int i = 0; i < vector1.length; i++) result += Math.pow((vector1[i] - vector2[i]) / divider[i], 2);
        return result;
    }

    private static float matricesDistance(float[][] matrices1, float[][] matrices2)
    {
        float result = 0f;
        if(matrices1.length != matrices2.length) return Float.MAX_VALUE;
        for(int i = 0; i < matrices1.length; i++){
            if(matrices1[i].length != matrices2[i].length) return Float.MAX_VALUE;
            result += vectorSquareDistance(matrices1[i], matrices2[i]);
        }
        float temp = 0f;
        temp += Math.pow(result, 0.5f);
        return temp;
    }
}
