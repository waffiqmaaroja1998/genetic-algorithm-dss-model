import java.io.*;
import java.util.Scanner;

public class MainProgram
{
    private static final String ROOT = "E:\\data_F\\Priv\\Algoritma Genetika\\Project\\";

    private static final int dataTestCount = 200;
    private static final int referenceCount = 30;
    private static final int criteriaCount = 9;
    private static final int initWeightCount = 50;

    private static int individualCount, totalGeneration;
    private static float mutationRate;

    private static float[][] parseData(String filepath, int row, int col)
    {
        File file = new File(filepath);
        float[][] data_parse = new float[row][col];
        try {
            String str;
            int k = 0;
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((str = br.readLine()) != null){
                str = str.replace("(", "");
                str = str.replace(")", "");
                str = str.replace(",", "");
                String[] sspl = str.split(" ");
                if(sspl.length != col) throw new Exception();
                for(int i=0; i<col; i++){
                    data_parse[k][i] = Float.parseFloat(sspl[i]);
                }
                k++;
            }
            if(k != row) throw new Exception();
        }catch (Exception e) {
            return null;
        }
        return data_parse;
    }

    private static boolean[] parseCategory(String filepath, int categoryCount)
    {
        File file = new File(filepath);
        boolean[] data_parse = new boolean[categoryCount];
        try {
            String str;
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((str = br.readLine()) != null){
                str = str.replace("(", "");
                str = str.replace(")", "");
                str = str.replace(",", "");
                String[] sspl = str.split(" ");
                if(sspl.length != categoryCount) throw new Exception();
                for(int i=0; i<categoryCount; i++){
                    data_parse[i] = Boolean.parseBoolean(sspl[i]);
                }
            }
        }catch (Exception e) {
            return null;
        }
        return data_parse;
    }

    private static void generateText(String filepath, float[][] datas, int row, int col)
    {
        File file = new File(filepath);

        try{
            if (file.createNewFile())
            {
                System.out.println("File is created!");
            } else {
                file.delete();
                file = new File(filepath);
                file.createNewFile();
                System.out.println("File is created!");
            }

            FileWriter writer = new FileWriter(file);

            for(int i = 0; i < row; i++){
                String s = "(";
                for(int j = 0; j < col; j++){
                    if(j != 0) s += ", ";
                    s += String.valueOf(datas[i][j]);
                }
                s += ")\n";
                writer.write(s);
            }
            writer.close();
            System.out.println("Success");
        }
        catch (Exception e){
            System.out.println("ERROR");
        }
    }

    public static void main(String[] args)
    {
        float[][] reference_parse = parseData(String.format("%sgen_y.txt", ROOT), referenceCount, criteriaCount);
        float[][] datas_parse = parseData(String.format("%sgen_test.txt", ROOT), dataTestCount, criteriaCount);
        boolean[] positive_parse = parseCategory(String.format("%skriteria.txt", ROOT), criteriaCount);
        float[][] weight_parse = parseData(String.format("%sinit_chromosome.txt", ROOT), initWeightCount, criteriaCount);

//        individualCount = 50;
//        mutationRate = 0.02f;
//        totalGeneration = 100000;

        DSS designSystem;
        Population population;
        designSystem = new DSS(criteriaCount, datas_parse, positive_parse);

        Scanner input = new Scanner(System.in);
        if(datas_parse != null && reference_parse != null && positive_parse != null)
        {
            System.out.println("Simulasi Algoritma Genetika");
            int p1 = choiceJenis(1, input);
            switch (p1){
                case 1:
                    individualCount = choiceIndividuCount(2, input);
                    mutationRate = choiceMutationRate(3, input);
                    totalGeneration = choiceGenerationCount(4, input);
                    if(isValidInput(individualCount, mutationRate, totalGeneration)){
                        population = new Population(criteriaCount, reference_parse, individualCount, mutationRate, designSystem);
                        simulation(population);
                    }
                    else System.out.println("Input anda salah !");
                    break;
                case 2:
                    individualCount = 50;
                    mutationRate = choiceMutationRate(2, input);
                    totalGeneration = choiceGenerationCount(3, input);
                    if(isValidInput(individualCount, mutationRate, totalGeneration)){
                        population = new Population(weight_parse, reference_parse, mutationRate, designSystem);
                        simulation(population);
                    }
                    else System.out.println("Input anda salah !");
                    break;
                default:
                    System.out.println("Input anda salah !");
                    break;
            }
//            population = new Population(criteriaCount, reference_parse, individualCount, mutationRate, designSystem);
//            population = new Population(weight_parse, reference_parse, mutationRate, designSystem);
        }
        else System.out.println("Parsing data failed");
    }

    private static int choiceJenis(int ind, Scanner input)
    {
        System.out.println(String.format("%d) Pilih jenis inisialisasi kromosom", ind));
        System.out.println("   1. Random");
        System.out.println("   2. Ditentukan (banyak kromosom = 50)");
        System.out.print("   Pilihan : ");
        try{
            String str = input.next();
            return Integer.parseInt(str);
        }
        catch (Exception e){
            return -1;
        }
    }

    private static int choiceIndividuCount(int ind, Scanner input)
    {
        System.out.println(String.format("%d) Pilih banyak individu awal", ind));
        System.out.print(  "   Pilihan : ");
        try{
            String str = input.next();
            return Integer.parseInt(str);
        }
        catch (Exception e){
            return -1;
        }
    }

    private static float choiceMutationRate(int ind, Scanner input)
    {
        System.out.println(String.format("%d) Pilih besar kemungkinan mutasi", ind));
        System.out.print(  "   Pilihan : ");
        try{
            String str = input.next();
            return Float.parseFloat(str);
        }
        catch (Exception e){
            return -1f;
        }
    }

    private static int choiceGenerationCount(int ind, Scanner input)
    {
        System.out.println(String.format("%d) Pilih banyak generasi", ind));
        System.out.print(  "   Pilihan : ");
        try{
            String str = input.next();
            return Integer.parseInt(str);
        }
        catch (Exception e){
            return -1;
        }
    }

    private static boolean isValidInput(int p1, float p2, int p3)
    {
        return (p1 > 0 && p2 > 0f && p3 > 0);
    }

    private static void simulation(Population population)
    {
        float currentFitness = 0f;
        int accumulativeGeneration = 1;
        for (int i = 0; i < totalGeneration; i++){
            population.calculateAllFitness();
            float temp = population.getBestFitness();
            if(currentFitness != temp){
                System.out.print(String.format("Generation %d (Best Fitness) : ", i+1));
                System.out.println(temp);
                population.showBest();
                currentFitness = temp;
                accumulativeGeneration = 1;
            }
            else ++accumulativeGeneration;
            population.selection();
        }
        System.out.print(String.format("Pada generasi ke-%d, diperoleh nilai fitness tertinggi yaitu ", totalGeneration));
        System.out.println(currentFitness);

        population.calculateAllFitness();
        population.showBest();
        population.showFinalRank();
        System.out.println("SELESAI\n");

        generateFinalWeight(population.getAllChromosome(), population.getAllChromosome().length, criteriaCount);
    }

    private static void generateFinalWeight(float[][] weight, int row, int col)
    {
        generateText(String.format("%sfinal_chromosome.txt", ROOT), weight, row, col);
    }

    public static void generateTesting()
    {
        int criteriaCount = 9;
        int countData = 100;
        int filterCount = 30;

        float[][] data_parse = parseData(String.format("%sgen_pre.txt", ROOT), countData, criteriaCount);
        boolean[] positive_parse = parseCategory(String.format("%skriteria.txt", ROOT), criteriaCount);

        DSS model = new DSS(criteriaCount, data_parse, positive_parse);
        float[] weight = {9.5f, 9.4f, 9.1f, 9.0f, 8.5f, 8.4f, 7.5f, 7.0f, 6.9f};
        float[][] filterBest = model.getRecommendedData(weight, filterCount);

        generateText(String.format("%sgen_y.txt", ROOT), filterBest, filterCount, criteriaCount);
    }
}
