import java.util.Arrays;
import java.util.Comparator;

public class DSS
{
    private int criteriaCount;
    private boolean[] isPositiveCriteria;

    private int allDataCount;
    private float[][] allData;
    private float[][] normalizeAllData;

    private float[][] referenceData;

    private Compare compare;

    DSS(int criteriaCount, float[][] allData, boolean[] isPositiveCriteria)
    {
        this.criteriaCount = criteriaCount;
        this.allData = allData;
        this.allDataCount = allData.length;
        this.isPositiveCriteria = isPositiveCriteria;
        this.compare = new Compare();
        generateNormalizeAllData();
    }

    private void generateNormalizeAllData()
    {
        float[] normalizer = new float[criteriaCount];
        for(int c = 0; c < criteriaCount; c++){
            if(isPositiveCriteria[c]) normalizer[c] = 0f;
            else normalizer[c] = 20f;
        }
        for(int c = 0; c < criteriaCount; c++){
            for(int d = 0; d < allData.length; d++){
                if(isPositiveCriteria[c]) normalizer[c] = Math.max(normalizer[c], allData[d][c]);
                else normalizer[c] = Math.min(normalizer[c], allData[d][c]);
            }
        }
        normalizeAllData = new float[allData.length][criteriaCount];
        for(int c = 0; c < criteriaCount; c++){
            for(int d = 0; d < allData.length; d++){
                if(allData[d][c] == 0 || normalizer[c] == 0) normalizeAllData[d][c] = 0;
                else if(isPositiveCriteria[c]) normalizeAllData[d][c] = allData[d][c] / normalizer[c];
                else normalizeAllData[d][c] = normalizer[c] / allData[d][c];
            }
        }
    }

    float[][] getRecommendedData(float[] weightUsed, int selectedCount)
    {
        float[][] recommended = new float[selectedCount][criteriaCount];
        int[] indeks = generateRecommendation(weightUsed, selectedCount);
        for (int i = 0; i < selectedCount; i++) recommended[i] = allData[indeks[i]];
        return recommended;
    }

    private int[] generateRecommendation(float[] weight, int selectedCount)
    {
        MyPair[] alternative = new MyPair[normalizeAllData.length];
        for(int i = 0; i < normalizeAllData.length; i++){
            float temp = vectorDotProduct(normalizeAllData[i], weight);
            alternative[i] = new MyPair(i, temp);
        }
        Arrays.sort(alternative, compare);
        int[] indexSelected = new int[selectedCount];
        for(int i = 0; i < selectedCount; i++){
            indexSelected[i] = alternative[i].indeks;
        }
        return indexSelected;
    }

    private float vectorDotProduct(float[] vector1, float[] vector2)
    {
        float result = 0f;
        if(vector1.length != vector2.length) return result;
        for(int i = 0; i < vector1.length; i++) result += vector1[i] * vector2[i];
        return result;
    }
}

class MyPair
{
    int indeks;
    float value;

    public MyPair(int indeks, float value) {
        this.indeks = indeks;
        this.value = value;
    }
}

class Compare implements Comparator<MyPair>
{
    @Override
    public int compare(MyPair o1, MyPair o2) {
        float temp = o2.value - o1.value;
        int res = 0;
        if(temp < 0) res = -1;
        else if(temp > 0) res = 1;
        return res;
    }
}
