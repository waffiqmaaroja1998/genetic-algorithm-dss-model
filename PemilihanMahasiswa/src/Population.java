import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Population
{
    private static final int BEST_SELECTED = 10;
    private static final int WORST_SELECTED = 10;

    private int referenceCount;
    private float[][] referenceData;

    private int individualCount;
    private float mutationRate;

    private Individual[] individuals;

    private DSS designSystem;
    private CompareByFitness compare;
    private Random rd;
    private float[] cdfFitness;

    Population(int criteriaCount, float[][] referenceData, int individualCount, float mutationRate, DSS designSystem)
    {
        this.referenceData = referenceData;
        this.referenceCount = referenceData.length;

        this.individualCount = individualCount;
        this.mutationRate = mutationRate;

        this.designSystem = designSystem;
        this.compare = new CompareByFitness();
        this.rd = new Random();
        individuals = new Individual[individualCount];
        for (int i = 0; i < individualCount; i++){
            individuals[i] = new Individual(criteriaCount, mutationRate, Math.sqrt(9.0 * referenceCount));
        }
    }

    Population(float[][] weightInit, float[][] referenceData, float mutationRate, DSS designSystem)
    {
        this.referenceData = referenceData;
        this.referenceCount = referenceData.length;

        this.individualCount = weightInit.length;
        this.mutationRate = mutationRate;

        this.designSystem = designSystem;
        this.compare = new CompareByFitness();
        this.rd = new Random();
        individuals = new Individual[individualCount];
        for (int i = 0; i < individualCount; i++){
            individuals[i] = new Individual(weightInit[i], mutationRate, Math.sqrt(9.0 * referenceCount));
        }
    }

    void calculateAllFitness()
    {
        for(int i = 0; i < individualCount; i++){
            float[][] recommendationResult = designSystem.getRecommendedData(individuals[i].getChromosome(), referenceCount);
            float distance = DistanceCalculation.calculateDistance(referenceData, recommendationResult);
            individuals[i].setFitness(distance);
        }
        sortByFitness();
    }

    private void sortByFitness()
    {
        Arrays.sort(individuals, compare);
    }

    float getBestFitness()
    {
        return individuals[0].getFitness();
    }

    void showBest()
    {
        System.out.print("-> Best Chromosome : ");
        System.out.println(individuals[0]);
    }

    void showFinalRank()
    {
        System.out.println("Final Rank :");
        float[][] recommendationResult = designSystem.getRecommendedData(individuals[0].getChromosome(), referenceCount);
        for(int i=0; i<recommendationResult.length; i++){
            System.out.print((i+1) + ") ");
            float[] values = recommendationResult[i];
            String str = "[";
            for(int j=0; j<values.length; j++){
                if(j>0) str += "; ";
                str += values[j];
            }
            str += "]";
            System.out.println(str);
        }
    }

//    void printAllFitness()
//    {
//        for(Individual i : individuals){
//            System.out.println(i.getFitness());
//        }
//    }

    void selection()
    {
        int topIndividual = (BEST_SELECTED * individualCount)/100;
        int bottomIndividual = (WORST_SELECTED * individualCount)/100;
        int avgIndividual = individualCount - topIndividual - bottomIndividual;

        cdfFitness = new float[individualCount];
        float total = 0f;
        for(int i = 0; i < individualCount; i++){
            total += individuals[i].getFitness();
            cdfFitness[i] = total;
        }
        for(int i=0; i<individualCount; i++){
            cdfFitness[i] /= total;
        }

        Individual[] newIndividuals = new Individual[individualCount];
        for(int i = 0; i < topIndividual; i++){
            newIndividuals[i] = individuals[i];
        }

        for(int i = topIndividual; i < topIndividual + avgIndividual; i++){
            int p1 = chooseParent(), p2 = chooseParent();
            newIndividuals[i] = individuals[p1].getCrossover(individuals[p2]);
        }

        for(int i = individualCount - bottomIndividual; i < individualCount; i++){
            newIndividuals[i] = individuals[i];
        }

        replacePopulation(newIndividuals);
    }

    float[][] getAllChromosome()
    {
        float[][] result = new float[individualCount][];
        for(int i = 0; i < individualCount; i++){
            result[i] = individuals[i].getChromosome();
        }
        return result;
    }

    private int chooseParent()
    {
        float prob = rd.nextFloat();
        for(int i = 0; i < individualCount; i++){
            if(prob <= cdfFitness[i]) return i;
        }
        return 0;
    }

    private void replacePopulation(Individual[] newIndividuals)
    {
        this.individuals = newIndividuals;
    }
}

class CompareByFitness implements Comparator<Individual>
{
    @Override
    public int compare(Individual o1, Individual o2) {
        float temp = o2.getFitness() - o1.getFitness();
        int res = 0;
        if(temp < 0) res = -1;
        else if(temp > 0) res = 1;
        return res;
    }
}


