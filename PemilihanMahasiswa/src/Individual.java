import java.util.Random;

public class Individual
{
    private double MAX_ERROR;
    private int genCount;
    private float[] chromosome;
    private float fitness;
    private float mutationRate;
    private Random rd;

    Individual(int genCount, float mutationRate, double maxError)
    {
        this.genCount = genCount;
        this.mutationRate = mutationRate;
        rd = new Random();
        this.chromosome = initialChromosome();
        this.MAX_ERROR = maxError;
        fitness = 0f;
    }

    Individual(float[] chromosome, float mutationRate, double maxError)
    {
        this.genCount = chromosome.length;
        this.mutationRate = mutationRate;
        rd = new Random();
        this.chromosome = chromosome;
        this.MAX_ERROR = maxError;
        fitness = 0f;
    }

    private float[] initialChromosome()
    {
        float[] temp = new float[this.genCount];
        for(int i = 0; i < this.genCount; i++){
            temp[i] = rd.nextFloat();
        }
        return temp;
    }

    private int[] mutationPoint()
    {
        boolean mutated = (rd.nextFloat() < mutationRate);
        if(mutated){
            int left = rd.nextInt(genCount);
            int right = (left == genCount-1) ? left : left + 1 + rd.nextInt(genCount-left-1);
            int[] temp = {left, right};
            return temp;
        }
        int[] temp = {-1, -1};
        return temp;
    }

    private float getRandomMutationGen()
    {
        return (rd.nextInt()%2 == 0) ? 0f : 1f;
    }

    float[] getChromosome() {
        return chromosome;
    }

    float getFitness() {
        return fitness;
    }

    void setFitness(float distance)
    {
        fitness = (float)(100f * (1f - distance/MAX_ERROR));
        //fitness =  500f/(5 + distance);
    }

    Individual getCrossover(Individual other)
    {
        float alpha = rd.nextFloat();
        float[] offspring = new float[genCount];
        for(int i = 0; i < genCount; i++) offspring[i] = (alpha * chromosome[i] + (1 - alpha) * other.chromosome[i]);
        int[] mutationPoint = mutationPoint();
        if(mutationPoint[0] >= 0){
            for(int i = mutationPoint[0]; i <= mutationPoint[1]; i++)
                offspring[i] = getRandomMutationGen();
        }
        return new Individual(offspring, mutationRate, MAX_ERROR);
    }

    @Override
    public String toString() {
        String str = "";
        str += "{";
        for(int i=0; i<genCount; i++){
            if(i>0) str += "; ";
            str += chromosome[i];
        }
        str += "}";
        return str;
    }
}
