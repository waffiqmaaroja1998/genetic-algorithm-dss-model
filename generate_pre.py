# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 18:36:23 2019

@author: My Computer
"""
import random

'''
parameter = [10.0, 10.0, 10.0, 5.0, 3.0, 4.0, 1.0, 3.0, 1.0]

txt = open(r"gen_pre.txt", "w")
cnt = 100

for i in range(cnt):
    lst = [round(random.uniform(0.6*k, k), 2) for k in parameter]
    linee = "("
    for x in range(len(lst)):
        if x != 0:
            linee += ", "
        linee += str(lst[x])
    linee += ")\n"
    txt.writelines(linee)

txt.close()

txt = open(r"gen_test.txt", "w")
cnt = 200

for i in range(cnt):
    lst = [round(random.uniform(0.6*k, k), 2) for k in parameter]
    linee = "("
    for x in range(len(lst)):
        if x != 0:
            linee += ", "
        linee += str(lst[x])
    linee += ")\n"
    txt.writelines(linee)

txt.close()
'''

txt = open(r"init_chromosome.txt", "w")
cnt = 50

for i in range(cnt):
    lst = [round(random.uniform(0.0, 1.0), 3) for k in range(9)]
    linee = "("
    for x in range(len(lst)):
        if x != 0:
            linee += ", "
        linee += str(lst[x])
    linee += ")\n"
    txt.writelines(linee)

txt.close()